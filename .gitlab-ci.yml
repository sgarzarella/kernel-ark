---
# This CI will only work for project members. CI for public contributors
# runs via a webhook on the merge requests. There's nothing you have to do if
# you want your changes tested -- created pipeline will be automatically
# linked in the merge request and appropriate labels will be added to it.
# Changes to this file will NOT be reflected in the webhook testing.

include:
  - project: cki-project/pipeline-definition
    ref: production
    file: kernel_templates.yml
  - project: cki-project/cki-lib
    ref: production
    file: .gitlab/ci_templates/interruptible.yml
    rules: [{if: $CI_PIPELINE_SOURCE == "merge_request_event"}]

workflow:
  name: $PIPELINE_NAME
  rules:
    # Rawhide release pipelines
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      variables:
        PIPELINE_NAME: 'Scheduled pipeline with RAWHIDE_RELEASE=$RAWHIDE_RELEASE'
    # when triggered as a multi-project pipeline for an MR
    - if: $CI_PIPELINE_SOURCE == 'pipeline' &&
          $PARENT_MERGE_REQUEST_IID != null && $PARENT_MERGE_REQUEST_IID != "" &&
          $CI_PROJECT_PATH =~ /^cki-project.kernel-ark/
      variables:
        PIPELINE_NAME: 'Downstream pipeline for $PARENT_PROJECT_PATH!$PARENT_MERGE_REQUEST_IID'
    # when triggered as a multi-project pipeline
    - if: $CI_PIPELINE_SOURCE == 'pipeline' &&
          $CI_PROJECT_PATH =~ /^cki-project.kernel-ark/
      variables:
        PIPELINE_NAME: 'Downstream pipeline for $PARENT_PROJECT_PATH'
    # HEAD pipelines
    - if: $CI_PIPELINE_SOURCE =~ /push|web/ &&
          $CI_PROJECT_PATH =~ /^cki-project.kernel-ark/
    # merge requests
    - if: $CI_MERGE_REQUEST_PROJECT_PATH =~ /^cki-project.kernel-ark/

.rules:
  only-os-build-automotive-devel-mr: &only-os-build-automotive-devel-mr
    if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME != "os-build-automotive-devel"
    when: never
  only-os-build-mr: &only-os-build-mr
    if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME != "os-build"
    when: never
  only-os-build-head: &only-os-build-head
    if: $CI_COMMIT_BRANCH != "os-build" || $CI_PIPELINE_SOURCE !~ /push|web/
    when: never
  only-os-build-head-mr: &only-os-build-head-mr
    if: ($CI_MERGE_REQUEST_TARGET_BRANCH_NAME != "os-build") &&
        ($CI_COMMIT_BRANCH != "os-build" || $CI_PIPELINE_SOURCE !~ /push|web/)
    when: never
  only-os-build-schedule: &only-os-build-schedule
    if: $CI_COMMIT_BRANCH != "os-build" || $CI_PIPELINE_SOURCE != "schedule"
    when: never
  only-ark-latest-head: &only-ark-latest-head
    if: $CI_COMMIT_BRANCH != "ark-latest" || $CI_PIPELINE_SOURCE !~ /push|web/
    when: never
  only-ark-latest-head-mr: &only-ark-latest-head-mr
    if: ($CI_MERGE_REQUEST_TARGET_BRANCH_NAME != "os-build") &&
        ($CI_COMMIT_BRANCH != "ark-latest" || $CI_PIPELINE_SOURCE !~ /push|web/)
    when: never
  only-cki-gating-head: &only-cki-gating-head
    if: $CI_COMMIT_BRANCH != "cki-gating" || $CI_PIPELINE_SOURCE !~ /pipeline/ || $builder_image !~ $JOB_FILTER
    when: never
  on-success: &on-success
    when: on_success
  manual-on-mr: &manual-on-mr
    if: $CI_PIPELINE_SOURCE == "merge_request_event"
    when: manual
    allow_failure: true
  manual-for-bot: &manual-for-bot
    if: $CI_COMMIT_AUTHOR =~ /<kernel-team@fedoraproject.org>/
    when: manual
    allow_failure: true
  srpm-for-bot: &srpm-for-bot
    if: $CI_COMMIT_AUTHOR =~ /<kernel-team@fedoraproject.org>/
    when: on_success
    variables:
      skip_build: 'true'
      skip_publish: 'true'
      architectures: ''

.pipeline:
  stage: test
  trigger:
    project: redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors
    strategy: depend
  variables:
    srpm_make_target: dist-srpm

.trigger_rawhide:
  extends: .pipeline
  trigger:
    branch: rawhide-ark
  variables:
    name: fedora-rawhide
    builder_image: quay.io/cki/builder-rawhide
    native_tools: 'true'
    kpet_tree_name: rawhide

.trigger_eln:
  extends: .pipeline
  trigger:
    branch: eln-ark
  variables:
    name: fedora-eln
    builder_image: quay.io/cki/builder-eln
    native_tools: 'true'
    kpet_tree_name: eln
    disttag_override: '.eln'

.trigger_c10s:
  extends: .pipeline
  trigger:
    branch: c10s
  variables:
    name: c10s
    builder_image: quay.io/cki/builder-stream10
    native_tools: 'true'
    kpet_tree_name: c10s

.cki_gating_overrides:
  variables:
    # prevent the CKI production machinery from caring about these pipelines
    CKI_DEPLOYMENT_ENVIRONMENT: retrigger
    # explicitly pass down some trigger variables to the child pipeline
    builder_image_tag: $builder_image_tag

.no_tests:
  variables:
    skip_setup: 'true'
    skip_test: 'true'
    skip_results: 'true'

.reported_tests:
  variables:
    report_rules: |
      [
        {"when": "failed_tests", "send_to": "failed_tests_maintainers"},
        {"when": "always", "send_to": "ark-team@redhat.com"}
      ]

.reported_tests_clang:
  variables:
    report_rules: |
      [
        {"when": "failed_tests", "send_to": "failed_tests_maintainers"},
        {"when": "always", "send_to": ["tstellar@redhat.com", "tbaeder@redhat.com"]}
      ]

.srpm_for_bot:
  rules:
    - *only-os-build-mr
    - *srpm-for-bot
    - *on-success

.manual_for_bot:
  rules:
    - *only-os-build-mr
    - *manual-for-bot
    - *on-success

.os_build_automotive_devel_mr:
  rules:
    - *only-os-build-automotive-devel-mr
    - *on-success

.ark_latest_head:
  rules:
    - *only-ark-latest-head
    - *on-success

.cki_gating_head:
  rules:
    - *only-cki-gating-head
    - *on-success

.merge_ark_latest:
  variables:
    merge_branch: ark-latest

# variants
.rawhide_up:
  variables:
    rpmbuild_with: up base
    package_name: kernel
    architectures: x86_64 aarch64 s390x ppc64le
    run_redhat_self_test: 'true'
.rawhide_up_debug:
  variables:
    rpmbuild_with: up debug
    package_name: kernel-debug
    architectures: x86_64 aarch64
.rawhide_16k:
  variables:
    rpmbuild_with: arm64_16k base
    package_name: kernel-16k
    architectures: aarch64
.rawhide_16k_debug:
  variables:
    rpmbuild_with: arm64_16k debug
    package_name: kernel-16k-debug
    architectures: aarch64
.rawhide_clang_up:
  variables:
    rpmbuild_with: up base
    package_name: kernel
    architectures: x86_64 aarch64 s390x ppc64le
    compiler: clang
.rawhide_clang_up_debug:
  variables:
    rpmbuild_with: up debug
    package_name: kernel-debug
    architectures: x86_64 aarch64
    compiler: clang
.rawhide_clanglto_up:
  variables:
    rpmbuild_with: clang_lto up base
    package_name: kernel
    architectures: x86_64 aarch64
    compiler: clang
.rawhide_clanglto_up_debug:
  variables:
    rpmbuild_with: clang_lto up debug
    package_name: kernel-debug
    architectures: x86_64 aarch64
    compiler: clang
.eln_up:
  variables:
    rpmbuild_with: up zfcpdump base
    package_name: kernel
    architectures: x86_64 aarch64 s390x ppc64le
    run_redhat_self_test: 'true'
.eln_up_debug:
  variables:
    rpmbuild_with: up debug
    package_name: kernel-debug
    architectures: x86_64 aarch64 s390x ppc64le
.eln_clang_up:
  variables:
    rpmbuild_with: up base
    package_name: kernel
    architectures: x86_64 aarch64 s390x ppc64le
    compiler: clang
.eln_clang_up_debug:
  variables:
    rpmbuild_with: up debug
    package_name: kernel-debug
    architectures: x86_64 aarch64 s390x ppc64le
    compiler: clang
  eln-rt: &eln-rt
.eln_rt:
  variables:
    rpmbuild_with: realtime base
    package_name: kernel-rt
    architectures: x86_64
.eln_rt_debug:
  variables:
    rpmbuild_with: realtime debug
    package_name: kernel-rt-debug
    architectures: x86_64
.eln_64k:
  variables:
    rpmbuild_with: arm64_64k base
    package_name: kernel-64k
    architectures: aarch64
.eln_64k_debug:
  variables:
    rpmbuild_with: arm64_64k debug
    package_name: kernel-64k-debug
    architectures: aarch64
.eln_automotive:
  variables:
    rpmbuild_with: automotive base
    package_name: kernel-automotive
    architectures: x86_64 aarch64
.eln_automotive_debug:
  variables:
    rpmbuild_with: automotive debug
    package_name: kernel-automotive
    architectures: x86_64 aarch64

# Rawhide CI
rawhide_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .srpm_for_bot,
            .rawhide_up]

rawhide_debug_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .manual_for_bot,
            .rawhide_up_debug]

rawhide_16k_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .manual_for_bot,
            .rawhide_16k]

rawhide_16k_debug_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .manual_for_bot,
            .rawhide_16k_debug]

rawhide_clang_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .manual_for_bot,
            .rawhide_clang_up]

rawhide_clang_debug_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .manual_for_bot,
            .rawhide_clang_up_debug]

rawhide_clanglto_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .manual_for_bot,
            .rawhide_clanglto_up]

rawhide_clanglto_debug_merge_request:
  extends: [.merge_request, .trigger_rawhide, .no_tests, .merge_ark_latest, .manual_for_bot,
            .rawhide_clanglto_up_debug]

# Rawhide baselines
rawhide_baseline:
  extends: [.baseline, .trigger_rawhide, .reported_tests, .ark_latest_head,
            .rawhide_up]

rawhide_debug_baseline:
  extends: [.baseline, .trigger_rawhide, .reported_tests, .ark_latest_head,
            .rawhide_up_debug]

rawhide_16k_baseline:
  extends: [.baseline, .trigger_rawhide, .no_tests, .ark_latest_head,
            .rawhide_16k]

rawhide_16k_debug_baseline:
  extends: [.baseline, .trigger_rawhide, .no_tests, .ark_latest_head,
            .rawhide_16k_debug]

rawhide_clang_baseline:
  extends: [.baseline, .trigger_rawhide, .reported_tests_clang, .ark_latest_head,
            .rawhide_clang_up]

rawhide_clang_debug_baseline:
  extends: [.baseline, .trigger_rawhide, .reported_tests_clang, .ark_latest_head,
            .rawhide_clang_up_debug]

rawhide_clanglto_baseline:
  extends: [.baseline, .trigger_rawhide, .reported_tests_clang, .ark_latest_head,
            .rawhide_clanglto_up]

rawhide_clanglto_debug_baseline:
  extends: [.baseline, .trigger_rawhide, .reported_tests_clang, .ark_latest_head,
            .rawhide_clanglto_up_debug]

# Rawhide CKI container image gating
rawhide_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_up, .cki_gating_overrides]

rawhide_debug_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_up_debug, .cki_gating_overrides]

rawhide_16k_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_16k, .cki_gating_overrides]

rawhide_16k_debug_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_16k_debug, .cki_gating_overrides]

rawhide_clang_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_clang_up, .cki_gating_overrides]

rawhide_clang_debug_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_clang_up_debug, .cki_gating_overrides]

rawhide_clanglto_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_clanglto_up, .cki_gating_overrides]

rawhide_clanglto_debug_cki_gating:
  extends: [.baseline, .trigger_rawhide, .no_tests, .merge_ark_latest, .cki_gating_head,
            .rawhide_clanglto_up_debug, .cki_gating_overrides]

# ELN CI
eln_merge_request:
  extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .srpm_for_bot,
            .eln_up]

eln_debug_merge_request:
  extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_up_debug]

# eln_clang_merge_request:
#   extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .manual_for_bot,
#             .eln_clang_up]
#
# eln_clang_debug_merge_request:
#   extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .manual_for_bot,
#             .eln_clang_up_debug]

eln_realtime_merge_request:
  extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_rt]

eln_realtime_debug_merge_request:
  extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_rt_debug]

eln_64k_merge_request:
  extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_64k]

eln_64k_debug_merge_request:
  extends: [.merge_request, .trigger_eln, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_64k_debug]

# ELN baselines
eln_baseline:
  extends: [.baseline, .trigger_eln, .reported_tests, .ark_latest_head,
            .eln_up]

eln_debug_baseline:
  extends: [.baseline, .trigger_eln, .reported_tests, .ark_latest_head,
            .eln_up_debug]

# eln_clang_baseline:
#   extends: [.baseline, .trigger_eln, .reported_tests_clang, .ark_latest_head,
#             .eln_clang_up]
#
# eln_clang_debug_baseline:
#   extends: [.baseline, .trigger_eln, .reported_tests_clang, .ark_latest_head,
#             .eln_clang_up_debug]

eln_realtime_baseline:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_eln, .no_tests, .ark_latest_head,
            .eln_rt]

eln_realtime_debug_baseline:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_eln, .no_tests, .ark_latest_head,
            .eln_rt_debug]

eln_64k_baseline:
  extends: [.baseline, .trigger_eln, .reported_tests, .ark_latest_head,
            .eln_64k]

eln_64k_debug_baseline:
  extends: [.baseline, .trigger_eln, .reported_tests, .ark_latest_head,
            .eln_64k_debug]

# ELN CKI container image gating
eln_cki_gating:
  extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
            .eln_up, .cki_gating_overrides]

eln_debug_cki_gating:
  extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
            .eln_up_debug, .cki_gating_overrides]

# eln_clang_cki_gating:
#   extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
#             .eln_clang_up, .cki_gating_overrides]
#
# eln_clang_debug_cki_gating:
#   extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
#             .eln_clang_up_debug, .cki_gating_overrides]

eln_realtime_cki_gating:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
            .eln_rt, .cki_gating_overrides]

eln_realtime_debug_cki_gating:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
            .eln_rt_debug, .cki_gating_overrides]

eln_64k_cki_gating:
  extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
            .eln_64k, .cki_gating_overrides]

eln_64k_debug_cki_gating:
  extends: [.baseline, .trigger_eln, .no_tests, .cki_gating_head,
            .eln_64k_debug, .cki_gating_overrides]

# c10s CI
c10s_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .srpm_for_bot,
            .eln_up]

c10s_debug_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_up_debug]

# c10s_clang_merge_request:
#   extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .manual_for_bot,
#             .eln_clang_up]
#
# c10s_clang_debug_merge_request:
#   extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .manual_for_bot,
#             .eln_clang_up_debug]

c10s_realtime_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_rt]

c10s_realtime_debug_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_rt_debug]

c10s_64k_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_64k]

c10s_64k_debug_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .merge_ark_latest, .manual_for_bot,
            .eln_64k_debug]

# c10s baselines
c10s_baseline:
  extends: [.baseline, .trigger_c10s, .reported_tests, .ark_latest_head,
            .eln_up]

c10s_debug_baseline:
  extends: [.baseline, .trigger_c10s, .reported_tests, .ark_latest_head,
            .eln_up_debug]

# c10s_clang_baseline:
#   extends: [.baseline, .trigger_c10s, .reported_tests_clang, .ark_latest_head,
#             .eln_clang_up]
#
# c10s_clang_debug_baseline:
#   extends: [.baseline, .trigger_c10s, .reported_tests_clang, .ark_latest_head,
#             .eln_clang_up_debug]

c10s_realtime_baseline:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_c10s, .no_tests, .ark_latest_head,
            .eln_rt]

c10s_realtime_debug_baseline:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_c10s, .no_tests, .ark_latest_head,
            .eln_rt_debug]

c10s_64k_baseline:
  extends: [.baseline, .trigger_c10s, .reported_tests, .ark_latest_head,
            .eln_64k]

c10s_64k_debug_baseline:
  extends: [.baseline, .trigger_c10s, .reported_tests, .ark_latest_head,
            .eln_64k_debug]

# c10s CKI container image gating
c10s_cki_gating:
  extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
            .eln_up, .cki_gating_overrides]

c10s_debug_cki_gating:
  extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
            .eln_up_debug, .cki_gating_overrides]

# c10s_clang_cki_gating:
#   extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
#             .eln_clang_up, .cki_gating_overrides]
#
# c10s_clang_debug_cki_gating:
#   extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
#             .eln_clang_up_debug, .cki_gating_overrides]

c10s_realtime_cki_gating:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
            .eln_rt, .cki_gating_overrides]

c10s_realtime_debug_cki_gating:  # no tests as realtime code not present upstream
  extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
            .eln_rt_debug, .cki_gating_overrides]

c10s_64k_cki_gating:
  extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
            .eln_64k, .cki_gating_overrides]

c10s_64k_debug_cki_gating:
  extends: [.baseline, .trigger_c10s, .no_tests, .cki_gating_head,
            .eln_64k_debug, .cki_gating_overrides]

# automotive/c10s CI on os-build-automotive-devel
c10s_automotive_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .os_build_automotive_devel_mr,
            .eln_automotive]

c10s_automotive_debug_merge_request:
  extends: [.merge_request, .trigger_c10s, .no_tests, .os_build_automotive_devel_mr,
            .eln_automotive_debug]

# scheduled job
.scheduled_setup:
  image: quay.io/cki/builder-eln:production
  variables:
    GIT_DEPTH: "0"
    GIT_CLONE_PATH: $CI_BUILDS_DIR/$CI_CONCURRENT_ID/kernel-ark
  before_script:
    - echo "fastestmirror=true" >> /etc/dnf/dnf.conf
    - dnf -y install python3-gitlab git openssh-clients dnf-utils gnupg2
    - git config user.name "Fedora Kernel Team"
    - git config user.email "kernel-team@fedoraproject.org"
    - echo "$PYTHON_GITLAB_CONFIG" >> ~/.python-gitlab.cfg
    # Need SSH since the clone is set up without write access.
    - eval $(ssh-agent -s)
    - echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$GITLAB_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - git remote add gitlab https://gitlab.com/cki-project/kernel-ark.git
    - git remote set-url --push gitlab git@gitlab.com:cki-project/kernel-ark.git
    - gpg2 --import "$TORVALDS_GPG_KEY"
    - git checkout --track origin/master && git describe
    - git checkout --track origin/os-build && git describe
    - export PROJECT_ID="$CI_PROJECT_ID"

merge_upstream:
  extends: .scheduled_setup
  script:
    - DIST_PUSH=1 redhat/scripts/ci/ark-update-configs.sh
  retry: 2
  rules:
    - *only-os-build-schedule
    - if: $RAWHIDE_RELEASE == "false"

rawhide_release:
  extends: .scheduled_setup
  script:
    - git checkout --track origin/ark-latest && git describe
    - git checkout --track origin/ark-infra && git describe
    # make sure we are on correct code base before running script
    - git checkout os-build  && git describe
    - DIST_PUSH=1 redhat/scripts/ci/ark-create-release.sh
  retry: 2
  rules:
    - *only-os-build-schedule
    - if: $RAWHIDE_RELEASE == "true"

merge_rt_automotive:
  extends: .scheduled_setup
  script:
    - git remote add linux-rt-devel git://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git
    - git checkout --track origin/os-build-rt-devel && git describe
    - git checkout --track origin/os-build-automotive-devel && git describe
    # make sure we are on correct code base before running script
    - git checkout os-build  && git describe
    - DIST_PUSH=1 redhat/scripts/ci/ark-merge-rt.sh
  retry: 2
  rules:
    - *only-os-build-schedule
    - if: $RAWHIDE_RELEASE == "rt"

docs:
  image: quay.io/cki/cki-tools:production
  script:
    - pip install sphinx
    - cd redhat/docs/
    - make SPHINXOPTS="-W" html
  artifacts:
    paths:
      - redhat/docs/_build/html/
  rules:
    - *only-os-build-head-mr
    - *on-success

pages:
  image: quay.io/cki/cki-tools:production
  needs: [docs]
  script:
    - mv redhat/docs/_build/html/ public/
  artifacts:
    paths:
      - public
  rules:
    - *only-os-build-head-mr
    - *manual-on-mr
    - *on-success

tag-cki-gating:
  image: quay.io/cki/cki-tools:production
  stage: deploy
  script:
    - mkdir --mode 700 --parents ~/.ssh
    - echo "${GITLAB_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    - eval $(ssh-agent -s)
    - ssh-add -q - <<< "${PRIVATE_KEY}"
    - git push --force git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}.git HEAD:refs/heads/cki-gating
  rules:
    - *only-ark-latest-head-mr
    - *manual-on-mr
    - *on-success
